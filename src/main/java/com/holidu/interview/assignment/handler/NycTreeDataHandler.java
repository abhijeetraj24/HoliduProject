package com.holidu.interview.assignment.handler;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.holidu.interview.assignment.bean.TreeModel;
import com.holidu.interview.assignment.exception.DataParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Supplier;

/**
 * @author Abhijeet Raj
 *
 */

/**
 * This class defines all custom logic that manages the data
 * received from NewYorkCity Tree data provider.
 */
@Component
public class NycTreeDataHandler implements TreeDataHandler {

    @Autowired
    protected ObjectMapper objectMapper;

    private static final double FEET_TO_METERS = 0.305;

    @Override
    public Set<TreeModel> getAllTrees(Supplier<ObjectNode[]> treeDataSupplier) {

        try {
            return objectMapper.convertValue(treeDataSupplier.get(), new TypeReference<Set<TreeModel>>() {
            });
        } catch (IllegalArgumentException e) {
            throw new DataParseException(e.getMessage(), e);
        }
    }

    @Override
    public Map<String, Long> findTreesInsideTheCircle(Set<TreeModel> allTrees, Double x, Double y, Double radius) {
        ConcurrentMap<String, Long> concurrentMap = new ConcurrentHashMap<String, Long>();
        double radiusPower = Math.pow(metersToFeet(radius), 2);

        allTrees.parallelStream().filter(tree ->
                (Math.pow(tree.getxCordinate() - x, 2) + Math.pow(tree.getyCordinate() - y, 2)) <= radiusPower
        ).forEach(tree -> concurrentMap.put(tree.getSpcCommonNameTree(),
                concurrentMap.getOrDefault(tree.getSpcCommonNameTree(), 0L)+1));
        return concurrentMap;
    }

    private double metersToFeet(double meters){
        return meters * FEET_TO_METERS;
    }

	

	
}
