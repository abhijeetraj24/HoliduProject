package com.holidu.interview.assignment.bean;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Abhijeet Raj
 *
 */
/**
 *  Bean for a Tree entity
 */

public class TreeModel {
	
	@JsonProperty("x_sp")
	private Double xCordinate;
	
	@JsonProperty("y_sp")
	private Double yCordinate;
	
	@NotNull
	@JsonProperty("tree_id")
	private String treeId;
	
	@NotNull
	@JsonProperty("spc_common")
	private String spcCommonNameTree;
	
	

    public TreeModel(String treeId, String spcCommonNameTree, Double xCordinate, Double yCordinate) {
        this.treeId = treeId;
        this.spcCommonNameTree = spcCommonNameTree;
        this.xCordinate = xCordinate;
        this.yCordinate = yCordinate;
    }

    
    public TreeModel() {
    }
    
    
	public Double getxCordinate() {
		return xCordinate;
	}

	public void setxCordinate(Double xCordinate) {
		this.xCordinate = xCordinate;
	}

	public Double getyCordinate() {
		return yCordinate;
	}

	public void setyCordinate(Double yCordinate) {
		this.yCordinate = yCordinate;
	}

	public String getTreeId() {
		return treeId;
	}

	public void setTreeId(String treeId) {
		this.treeId = treeId;
	}

	public String getSpcCommonNameTree() {
		return spcCommonNameTree;
	}

	public void setSpcCommonNameTree(String spcCommonNameTree) {
		this.spcCommonNameTree = spcCommonNameTree;
	}

	
	
	
	

}
